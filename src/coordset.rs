// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: MPL-2.0

use ang::Angle;
use Angle::Degrees;
use num::{FromPrimitive, Signed, ToPrimitive, traits::{NumAssign, real::Real}};
use std::convert::{From, TryFrom};

pub trait CoordElem :
Copy
+ NumAssign
+ FromPrimitive
+ PartialOrd
+ Signed
+ ToPrimitive
{}

pub trait DisplacementT:
CoordElem
{}

pub trait BasisT:
CoordElem
+ Real
{}

pub trait AngleT:
CoordElem
{}

impl<T> CoordElem for T where T:
Copy
+ NumAssign
+ FromPrimitive
+ PartialOrd
+ Signed
+ ToPrimitive
{}

impl<T> DisplacementT for T where T:
CoordElem
{}

impl<T> BasisT for T where T:
CoordElem
+ Real
{}

impl<T> AngleT for T where T:
CoordElem
{}


#[derive(Clone, Copy)]
pub struct RangeCosine<R: DisplacementT, T: BasisT> {
    pub r : R,
    pub u : T,
    pub v : T,
    pub w : Option<T>,
}

#[derive(Clone, Copy)]
pub struct Cartesian<T: DisplacementT> {
    pub x : T,
    pub y : T,
    pub z : T,
}

#[derive(Clone, Copy)]
pub struct Polar<R: DisplacementT, T: AngleT> {
    pub rho : R,
    pub theta : Angle<T>,
    pub phi : Angle<T>,
}

#[derive(Clone, Copy)]
pub struct Geodetic<R: DisplacementT, T: AngleT> {
    pub lat: Angle<T>,
    pub lon: Angle<T>,
    pub alt: R,
}

impl<T: DisplacementT + BasisT> TryFrom<&Vec<T>> for RangeCosine<T, T> {
    type Error = &'static str;
    fn try_from(value: &Vec<T>) -> Result<Self, Self::Error> {
        if value.len() < 3 || 4 < value.len() {
            return Err("RUV(W) Vector must be of length 3 or 4.")
        }
        Ok(Self { r: value[0], u: value[1], v: value[2], w: value.get(3).copied() })
    }
}

impl<T: DisplacementT> TryFrom<&Vec<T>> for Cartesian<T> {
    type Error = &'static str;
    fn try_from(value: &Vec<T>) -> Result<Self, Self::Error> {
        if value.len() != 3 {
            return Err("Cartesian Vector must be of length 3.")
        }
        Ok(Self { x: value[0], y: value[1], z: value[2] })
    }
}

impl<T: DisplacementT + AngleT> TryFrom<&Vec<T>> for Polar<T, T> {
    type Error = &'static str;
    fn try_from(value: &Vec<T>) -> Result<Self, Self::Error> {
        if value.len() != 3 {
            return Err("Polar Vector must be of length 3.")
        }
        Ok(Self { rho: value[0], theta: Degrees(value[1]), phi: Degrees(value[2]) })
    }
}

impl<T: AngleT + DisplacementT> TryFrom<&Vec<T>> for Geodetic<T, T> {
    type Error = &'static str;
    fn try_from(value: &Vec<T>) -> Result<Self, Self::Error> {
        if value.len() != 3 {
            return Err("Geodetic Vector must be of length 3.")
        }
        Ok(Self { lat: Degrees(value[0]), lon: Degrees(value[1]), alt: value[2] })
    }
}

macro_rules! impl_from3 {
    ($($t:ty),+) => {
        // this is very lazy and likely won't survive
        $(impl<T: DisplacementT + BasisT + AngleT> From<[T; 3]> for $t {
            fn from(value: [T; 3]) -> Self {
                Self::try_from(&value.to_vec()).unwrap()
            }
        })+
    }
}

impl_from3!(RangeCosine<T, T>, Cartesian<T>, Polar<T, T>, Geodetic<T, T>);

impl<T: DisplacementT + BasisT> From<[T; 4]> for RangeCosine<T, T> {
    fn from(value: [T; 4]) -> Self {
        Self::try_from(&value.to_vec()).unwrap()
    }
}