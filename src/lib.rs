// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: MPL-2.0

pub mod coordset;
pub mod error;
mod pascal;
mod statevec;

pub use error::CoordsysError;
pub use statevec::*;


#[allow(unused_imports)]
#[cfg(test)]
mod tests {
    const SEED: u64 = 0;

    use std::{sync::{Arc, Mutex, atomic::*}, thread, time::{Duration, Instant}};
    use crate::{
        EcefVec, EciVec, EnuVec, FaceVec, LlaPos, RaeVec, RuvwVec
    };
    use rand_chacha::{ChaCha8Rng, rand_core::SeedableRng};
    use rand_distr::{Uniform, UnitSphere, Distribution};

    #[test]
    fn face_ruvw_face() {
        const RAN_MIN: f64 = 10.0;
        const RAN_MAX: f64 = 12_742_000.0;  // approx Earth diameter
        const VEL: f64 = 34_300.0;          // mach 100
        const ACC: f64 = 9_812.0;           // 1000 Gs
        const MAX_ERR: f64 = 1.0e-3;        // 1mm
        const NUM_SAMP: u32 = 65536;

        let range_dist = Uniform::new_inclusive(RAN_MIN, RAN_MAX);
        let vel_dist = Uniform::new_inclusive(0.0, VEL);
        let acc_dist = Uniform::new_inclusive(0.0, ACC);

        let num_thr = num_cpus::get() as u32 + 1;
        let samp_per_thr = (NUM_SAMP + num_thr - 1) / num_thr;
        let e_rss_max = Arc::new(Mutex::new(vec![0.0_f64, 0.0_f64, 0.0_f64]));
        let t_min_fore = Arc::new(Mutex::new(Duration::new(u64::MAX, 0)));
        let t_min_back = Arc::new(Mutex::new(Duration::new(u64::MAX, 0)));

        let mut thrs: Vec<thread::JoinHandle<()>> = Vec::new();
        thrs.reserve(num_thr as usize);
        for t in 0..num_thr {
            let e_rss_max = Arc::clone(&e_rss_max);
            let t_min_fore = Arc::clone(&t_min_fore);
            let t_min_back = Arc::clone(&t_min_back);
            thrs.push(thread::spawn(move || {
                let mut rng = ChaCha8Rng::seed_from_u64(SEED);
                rng.set_stream(t as u64);
                for i in 0..samp_per_thr {
                    if t as u32 * samp_per_thr + i >= NUM_SAMP { continue; }
                    let range = range_dist.sample(& mut rng);
                    let vel = vel_dist.sample(& mut rng);
                    let acc = acc_dist.sample(& mut rng);
                    let mut gen = |mag: f64| UnitSphere.sample(& mut rng).map(|i: f64| i * mag);
                    let _xyz1: FaceVec<f64> = FaceVec(vec![
                        gen(range).into(),
                        gen(vel).into(),
                        gen(acc).into(),
                    ]);

                    let inst0 = Instant::now();
                    let _ruvw1: RuvwVec<f64, f64> = _xyz1.clone().try_into().unwrap();
                    let inst1 = Instant::now();
                    let _xyz2: FaceVec<f64> = _ruvw1.try_into().unwrap();
                    let inst2 = Instant::now();

                    {
                        let mut t_fore = t_min_fore.lock().unwrap();
                        *t_fore = (*t_fore).min(inst1.duration_since(inst0));
                    } {
                        let mut t_back = t_min_back.lock().unwrap();
                        *t_back = (*t_back).min(inst2.duration_since(inst1));
                    }

                    let mut euclid = vec![0.0_f64, 0.0_f64, 0.0_f64];
                    for s in 0.._xyz2.0.len() {
                        euclid[s] = (num::pow(_xyz2.0[s].x - _xyz1.0[s].x, 2)
                            + num::pow(_xyz2.0[s].y - _xyz1.0[s].y, 2)
                            + num::pow(_xyz2.0[s].z - _xyz1.0[s].z, 2)).sqrt();
                    }
                    {
                        let mut e_rss_max = e_rss_max.lock().unwrap();
                        *e_rss_max = (*e_rss_max).iter().zip(euclid)
                            .map(|(&curr, new)| curr.max(new.sqrt())).collect();
                    }
                }
            }));
        }
        for thr in thrs {
            thr.join().unwrap();
        }

        let e_rss_max = (*e_rss_max.lock().unwrap()).clone();
        for s in e_rss_max { assert!(s < MAX_ERR) }

        println!("Fastest Coversions\nFace to RUVW: {:?}\nRUVW to Face: {:?}",
            *t_min_fore.lock().unwrap(), *t_min_back.lock().unwrap());
    }
}
