// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: MPL-2.0

#[derive(Debug)]
pub enum CoordsysError {
    // Name of CoordSet or StateVec, message
    ConstructError(String, Option<String>),
    // From StateVec, Into StateVec, message
    ConvertError(String, String, Option<String>),
    // Message
    MiscError(Option<String>),
    #[doc(hidden)]
    __Nonexhaustive,
}

impl CoordsysError {
    #[allow(dead_code)]
    fn message(&self) -> &Option<String> {
        match self {
            Self::ConstructError(_, msg) => msg,
            Self::ConvertError(_, _, msg) => msg,
            Self::MiscError(msg) => msg,
            Self::__Nonexhaustive => unreachable!()
        }
    }
}

impl std::error::Error for CoordsysError {}

impl std::fmt::Display for CoordsysError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        const HEADER: &str = "CoordSys Error";
        let details = |o: &Option<String>| -> String {
            o.as_ref().map_or(String::from("."), |m| format!(": {}", m))
        };
        match self {
            Self::ConstructError(typename, msg)
                => write!(f, "{HEADER}: Failed to construct {typename}{}", details(msg), HEADER=HEADER, typename=typename),
            Self::ConvertError(from, into, msg)
                => write!(f, "{HEADER}: Failed to convert {from}->{into}{}", details(msg), HEADER=HEADER, from=from, into=into),
            Self::MiscError(msg)
                => write!(f, "{HEADER}{}", details(msg), HEADER=HEADER),
            Self::__Nonexhaustive => unreachable!()
        }
    }
}