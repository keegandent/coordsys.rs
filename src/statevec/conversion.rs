// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: MPL-2.0
use crate::{CoordsysError, pascal::*};
use CoordsysError::*;
use super::{*, util::*};

use num::pow;
use std::any::type_name;

impl<R: DisplacementT + From<T>, T: BasisT> TryFrom<RuvwVec<R, T>> for FaceVec<R> {
    type Error = CoordsysError;
    fn try_from(value: RuvwVec<R, T>) -> Result<Self, Self::Error> {
        use CheckExistingW::*;
        let throw = |msg: Option<String>| -> Self::Error {
            return ConvertError(type_name::<RuvwVec<R, T>>().into(), type_name::<Self>().into(), msg);
        };

        let mut value = value.clone();
        if let Err(e) = value.fill_w(None, Some(Correct(2.0 * T::epsilon().to_f64().unwrap().sqrt()))) {
            return Err(e);
        }
        let _w = value.0[0].w.unwrap();
        let mut s: Self = Self(vec![]);
        s.0.reserve(value.0.len());
        for state_num in 0..(value.0.len() as i32) {
            let pas_vec: Vec<R> = pascal_triangle_row(state_num).unwrap();
            let mut state_n: Cartesian<R> = Cartesian {
                x: R::zero(),
                y: R::zero(),
                z: R::zero(),
            };
            for term_num in 0..=state_num {
                let value_n_minus_k = &value.0[(state_num - term_num) as usize];
                let coeff = pas_vec[term_num as usize] * value.0[term_num as usize].r;
                state_n.x += coeff * value_n_minus_k.u.into();
                state_n.y += coeff * value_n_minus_k.v.into();
                if let Some(w) = value_n_minus_k.w {
                    state_n.z += coeff * w.into();
                } else {
                    return Err(throw(None));
                }
            }
            s.0.push(state_n);
        }
        return Ok(s);
    }
}

impl<R: DisplacementT, T: BasisT + From<R>> TryFrom<FaceVec<R>> for RuvwVec<R, T> {
    type Error = CoordsysError;
    fn try_from(value: FaceVec<R>) -> Result<Self, Self::Error> {
        let throw = |msg: Option<String>| -> Self::Error {
            return ConvertError(type_name::<FaceVec<R>>().into(), type_name::<Self>().into(), msg);
        };

        let mut s: Self = Self(vec![]);
        s.0.reserve(value.0.len());
        for state_num in 0..(value.0.len() as i32) {
            let value_n = &value.0[state_num as usize];
            let mut state_n: RangeCosine<R, T> = RangeCosine {
                r: R::zero(),
                u: T::zero(),
                v: T::zero(),
                w: Some(T::zero()),
            };
            let state_0_r: R;

            if state_num == 0 {
                state_0_r = R::from_f64(sort_accum(vec![
                    pow(value_n.x, 2),
                    pow(value_n.y, 2),
                    pow(value_n.z, 2)]
                ).to_f64().unwrap().sqrt()).ok_or_else(|| throw(None))?;
                state_n.r = state_0_r;
            } else {
                state_0_r = s.0[0].r;
                let value_0 = &value.0[0];
                let pas_vec_n_minus_1: Vec<R> = pascal_triangle_row(state_num - 1).unwrap();
                state_n.r =
                    value_n.x * value_0.x
                    + value_n.y * value_0.y
                    + value_n.z * value_0.z;
                for term_num in 1..state_num {
                    let value_k = &value.0[term_num as usize];
                    let value_n_minus_k = &value.0[(state_num - term_num) as usize];
                    state_n.r += pas_vec_n_minus_1[term_num as usize] * (
                        value_k.x * value_n_minus_k.x
                        + value_k.y * value_n_minus_k.y
                        + value_k.z * value_n_minus_k.z
                        - s.0[term_num as usize].r * s.0[(state_num - term_num) as usize].r
                    );
                }
                state_n.r /= state_0_r;
            }

            let pas_vec_n: Vec<R> = pascal_triangle_row(state_num).unwrap();
            state_n.u = value_n.x.into();
            state_n.v = value_n.y.into();
            state_n.w = Some(value_n.z.into());
            for term_num in 1..=state_num {
                let state_n_minus_k = &s.0[(state_num - term_num) as usize];
                let coeff: T = (pas_vec_n[term_num as usize]
                    * s.0.get(term_num as usize).unwrap_or(&state_n).r).into();
                state_n.u -= coeff * state_n_minus_k.u;
                state_n.v -= coeff * state_n_minus_k.v;
                *state_n.w.as_mut().unwrap() -= coeff * state_n_minus_k.w.unwrap();
            }
            state_n.u /= state_0_r.into();
            state_n.v /= state_0_r.into();
            *state_n.w.as_mut().unwrap() /= state_0_r.into();

            s.0.push(state_n);
        }
        return Ok(s);
    }
}