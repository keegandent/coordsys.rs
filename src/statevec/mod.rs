// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: MPL-2.0
mod conversion;
mod util;

use crate::coordset::*;

#[repr(transparent)]
#[derive(Clone)]
pub struct RuvwVec<R: DisplacementT, T: BasisT>(pub Vec<RangeCosine<R, T>>);

#[repr(transparent)]
#[derive(Clone)]
pub struct FaceVec<T: DisplacementT>(pub Vec<Cartesian<T>>);

#[repr(transparent)]
#[derive(Clone)]
pub struct RaeVec<R: DisplacementT, T: AngleT>(pub Vec<Polar<R, T>>);

#[repr(transparent)]
#[derive(Clone)]
pub struct EnuVec<T: DisplacementT>(pub Vec<Cartesian<T>>);

#[repr(transparent)]
#[derive(Clone)]
pub struct EcefVec<T: DisplacementT>(pub Vec<Cartesian<T>>);

#[repr(transparent)]
#[derive(Clone)]
pub struct EciVec<T: DisplacementT>(pub Vec<Cartesian<T>>);

#[repr(transparent)]
#[derive(Clone)]
pub struct LlaPos<R: DisplacementT, T: AngleT>(pub Geodetic<R, T>);


pub trait StateVec<R: DisplacementT, A: AngleT, B: BasisT>:
TryFrom<RuvwVec<R, B>>
+ TryFrom<FaceVec<R>>
+ TryFrom<RaeVec<R, A>>
+ TryFrom<EnuVec<R>>
+ TryFrom<EcefVec<R>>
+ TryFrom<EciVec<R>>
+ TryFrom<LlaPos<R, A>> {}