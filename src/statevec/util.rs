// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: MPL-2.0
use crate::{CoordsysError, coordset::*, pascal::*};
use CoordsysError::*;
use super::RuvwVec;

use function_name::named;
use num::{abs, pow};

pub enum CheckExistingW {
    Skip,
    Overwrite,
    Throw(f64),
    Correct(f64),
}

pub(super) fn sort_accum<T: CoordElem>(mut v: Vec<T>) -> T {
    v.sort_unstable_by(|a, b| a.abs().partial_cmp(&b.abs()).unwrap());
    v.into_iter().reduce(|accum, item| accum + item).unwrap_or(T::zero())
}

impl<R: DisplacementT, T: BasisT> RuvwVec<R, T> {
    #[named]
    pub fn fill_w(&mut self, make_negative: Option<bool>, check: Option<CheckExistingW>) -> Result<(), CoordsysError> {
        use CheckExistingW::*;
        let throw = |msg: Option<String>| -> CoordsysError {
            return MiscError(Some(format!("{}{}", function_name!(), msg.map_or(".".to_string(),|m| format!(": {}", m)))));
        };

        let mut chex = check.unwrap_or(Skip);   // define the default in one place
        for state_num in 0..(self.0.len() as i32) {
            let state_n = &self.0[state_num as usize];
            if state_n.w.is_some() && matches!(chex, Skip) { continue; }

            let mut new_w: T;
            //  to minimize precision loss, use ~O(state_num) space to store summands
            let mut w_summands: Vec<T> = Vec::new();
            if state_num == 0 {
                w_summands.reserve(3);
                w_summands.extend_from_slice(&[-pow(state_n.u, 2), -pow(state_n.v, 2), T::one()]);
                new_w = sort_accum(w_summands);
                if new_w < T::zero() {
                    new_w = T::zero();
                }
                new_w = new_w.sqrt();
                new_w *= match make_negative.unwrap_or(state_n.w.unwrap_or(T::one()) < T::zero()) {
                    false => T::one(),
                    true => T::zero().sub(T::one()),
                };
            } else {
                let state_0 = &self.0[0];
                w_summands.reserve(state_num as usize * 3 + 2);
                w_summands.extend_from_slice(&[state_n.u * state_0.u, state_n.v * state_0.v]);
                let pas_vec: Vec<T> = pascal_triangle_row(state_num - 1).unwrap();
                for term_num in 1..state_num {
                    let state_k = &self.0[term_num as usize];
                    let state_n_minus_k = &self.0[(state_num - term_num) as usize];
                    w_summands.extend_from_slice(&[
                        pas_vec[term_num as usize] * state_k.u * state_n_minus_k.u,
                        pas_vec[term_num as usize] * state_k.v * state_n_minus_k.v,
                        pas_vec[term_num as usize] * state_k.w.unwrap() * state_n_minus_k.w.unwrap(),
                    ]);
                }
                new_w = sort_accum(w_summands);
                if state_0.w.unwrap() == T::zero() {
                    if state_n.w.is_some() {
                        // TODO: log warning that checking behavior is being overriden to skip
                        chex = Skip;
                        continue;
                    } else {
                        return Err(throw(Some("Divide by zero.".to_string())));
                    }
                }
                new_w /= -state_0.w.unwrap();
            }

            if state_n.w.is_some() {
                let out_of_bounds = |thresh: &f64| -> bool {
                    state_n.w.map(|w| {
                        abs((w - new_w).to_f64().unwrap()) > *thresh
                    }).unwrap()
                };
                match chex {
                    Throw(thr) => {
                        if out_of_bounds(&thr) {
                            return Err(MiscError(Some(format!(
                                "{}: Existing W in RuvwVec failed check.", function_name!()
                            ))))
                        } else { continue; }
                    }
                    Correct(thr) => if !out_of_bounds(&thr) { continue; } // fall through for out of bounds
                    _=> {} // Skip never gets here and Overwrite will fall through
                }
            }
            self.0[state_num as usize].w = Some(new_w);
        }
        return Ok(());
    }
}