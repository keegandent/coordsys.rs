// Copyright 2022 Keegan Dent.
// SPDX-License-Identifier: MPL-2.0
use num::FromPrimitive;

// this is slightly less ugly than it was
pub fn pascal_triangle_row<T: FromPrimitive>
        (row_idx: i32) -> Result<Vec<T>, &'static str> {
    if row_idx < 0 { return Err("Negative rows of Pascal's Triangle not (yet) supported."); }
    let mut row_vec: Vec<i32> = Vec::new();
    row_vec.reserve((row_idx + 1) as usize);
    for col_idx in 0..(row_idx + 1) {
        if col_idx == 0 { row_vec.push(1); continue; }
        let temp = row_idx - col_idx + 1;
        row_vec.push(row_vec[(col_idx - 1) as usize] * temp / col_idx);
    }
    let out: Result<Vec<T>, &'static str>
        = row_vec.iter().map(|&e| T::from_i32(e).ok_or("Failed conversion!")).collect();
    return out;
}

#[cfg(test)]
mod tests {
    use super::pascal_triangle_row;

    #[test]
    fn test() {
        let four_row: Vec<i8> = pascal_triangle_row(4).unwrap();
        assert_eq!(four_row, vec![1, 4, 6, 4, 1]);
        let five_row: Vec<f64> = pascal_triangle_row(5).unwrap();
        assert_eq!(five_row, vec![1.0, 5.0, 10.0, 10.0, 5.0, 1.0]);
    }
}